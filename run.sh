#!/bin/bash

source common-source.sh

# https://github.com/oyvindln/vhs-decode/compare/085d488515eaba27867f3bd2e4697e6face26c09...vhs_decode
echo "Running vhs-decode with commit $GIT_COMMIT, look here for changes since then:"
echo "https://github.com/oyvindln/vhs-decode/compare/${GIT_COMMIT}...vhs_decode"


add_mount="$1"

if [[ ! -d "$add_mount" ]] ; then
	echo "Need data path to mount to /mnt"
	exit 1
fi

add_mount="$(realpath "$add_mount")"

docker run -ti --rm \
	-u $(id -u):$(id -g) \
	-e "DISPLAY=$DISPLAY" \
	-e "XAUTHORITY=$XAUTHORITY" \
	-v /tmp/.X11-unix:/tmp/.X11-unix \
	--mount "type=bind,source=$XAUTHORITY,destination=$XAUTHORITY" \
	-v /dev/shm:/dev/shm \
	--privileged \
	--mount "type=bind,source=$add_mount,destination=/mnt" \
	${IMAGE_TAG}

