#!/bin/bash

source common-source.sh

docker build --pull --build-arg VHS_DECODE_COMMIT=${GIT_COMMIT} --tag ${IMAGE_TAG} docker/

if [[ $? == 0 ]] ; then
	echo "Succesfully build ${IMAGE_TAG}"
else
	echo "Failed to build ${IMAGE_TAG}"
fi
