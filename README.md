# [VHS-Decode][vhs-decode] in a docker container

A couple scripts to build and run a docker container with a specific version of [VHS-Decode][vhs-decode].
Enables you to easily have multiple versions in parallel, and make them portable across multiple machines ([docker save][docker-save] / [docker load][docker-load]).

To build clone this repo, and from within its root directory:
```bash
./build.sh
```
This will build for a while and result in an image like `own/vhs-decode:ea9fadd8`.
The short git commit is the revision of the [VHS-Decode][vhs-decode] source code.
There will be some simple self tests of the installation during the build. 


After building it you can run it with:
```bash
./run.sh /media/captures
```
This will start up a container and mount a local folder with your rf captures (e.g. `/media/captures`), to `/mnt` in the container.
[VHS-Decode][vhs-decode] is installed into `/opt/vhs-decode`.
Additionally X11 is also routed through so you can just run `ld-analyze` in the container and get the usual UI on your host machine.

[vhs-decode]: https://github.com/oyvindln/vhs-decode
[docker-save]: https://docs.docker.com/engine/reference/commandline/save/
[docker-load]: https://docs.docker.com/engine/reference/commandline/load/
